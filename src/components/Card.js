import React from 'react'
import Stories from "./Stories";
import Cards from "./Cards";
import '../styles/card.scss';

function Card() {
    const commnetsOne=[
        {
          user:"stivJobs",
          text:"whooou this is amazing",
          id:1
        },
        {
            user:"MackZuberburg",
            text:"Please you can join my team",
            id:2
        },
        {
            user:"MackZuberburg",
            text:"Please you can join my team",
            id:3
          },
          {
            user:"stivJobs",
            text:"whooou this is amazing",
            id:4
          },
          {
              user:"MackZuberburg",
              text:"Please you can join my team",
              id:5
          },
          {
              user:"MackZuberburg",
              text:"Please you can join my team",
              id:6
            }
        ]
    return (
        <div className='cards'>
            <Stories/>
            <Cards accountName='Binimabek' storyBorder={true} image='https://picsum.photos/800' comments={commnetsOne} likedByText="qalaysiz" likedByNumber={89} hours={16}/>
            <Cards accountName='Binimabek' storyBorder={true} image='https://picsum.photos/800/900' comments={commnetsOne} likedByText="qalaysiz" likedByNumber={89} hours={16}/>
            <Cards accountName='Binimabek' storyBorder={true} image='https://picsum.photos/800/1000' comments={commnetsOne} likedByText="qalaysiz" likedByNumber={89} hours={16}/>
            <Cards accountName='Binimabek' storyBorder={true} image='https://picsum.photos/800/1100' comments={commnetsOne} likedByText="qalaysiz" likedByNumber={89} hours={16}/>
            <Cards accountName='Binimabek' storyBorder={true} image='https://picsum.photos/800/1200' comments={commnetsOne} likedByText="qalaysiz" likedByNumber={89} hours={16}/>
        </div> 
    )
}

export default Card
