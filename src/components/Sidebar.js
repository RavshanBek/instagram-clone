import React from 'react'
import Sticky from "react-sticky-el";
import Profilie from "./Profile";
import Suggestions from './Suggestions';
import Footer from './Footer';
import image from '../images/ravshan.jpg';
import "../styles/sidebar.scss";
function Sidebar() {
    return (
            <Sticky topOffset={-80}>
               <div className="sidebar">
                   <Profilie
                    username='Brlosbek'
                    caption='ravshanbek'
                    urlText="Switch"
                    iconSize='big'
                    image={image}
                   />
                   <Suggestions/>
                   <Footer/>

               </div>
            </Sticky>
    )
}

export default Sidebar
