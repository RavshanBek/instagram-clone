import React from 'react'
import Profile from "./Profile";
import { ReactComponent as CardButton } from "../images/cardButton.svg";
import CardMenu from "./CardMenu";
import Comment from "./Comment";
import '../styles/cardsmini.scss'
function Cards(props) {
    const {
        storyBorder,
        image,
        comments,
        likedByText,
        likedByNumber,
        hours
    } = props
    return (
        <div className='cardsmini'>
            <header>
            <Profile 
             iconSize='medium'
             storyBorder={storyBorder} />
             <CardButton className='cardButton'/>
            </header>
             <img src={image} alt="card content" className='cardImage'/>
             <CardMenu/> 
             <div className="likedBy">
                 <Profile iconSize='small' hideAccountName={true}/>
                 <span>
                 liked by <strong>{likedByText} </strong> and {" "}
                 liked by <strong>{likedByNumber} others</strong>
                 </span>
             </div>
             <div className="comments">
                 {comments.map(commnets=>{
                     return(
                         <Comment 
                         key={commnets.id}
                         accountName={commnets.user}
                         comment={commnets.text}
                         />
                     )
                 })}
             </div>
             <div className="timePosted">
                 {hours} Hours Ago
             </div>
             <div className="addComment">
                 <div className="commentText">
                     Add a comment....
                 </div>
                 <div className="postText">POST</div>
             </div>
        </div>
    )
}

export default Cards
