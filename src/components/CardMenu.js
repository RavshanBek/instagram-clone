import React from 'react';
import {ReactComponent as BookMark} from "../images/bookMark.svg";
import {ReactComponent as Message} from "../images/message.svg";
import {ReactComponent as Likes} from "../images/likes.svg";
import {ReactComponent as Send} from "../images/send.svg";
import '../styles/cardmenu.scss'
function CardMenu() {
    return (
        <div className='cardMenu'>
            <div className="interaction">
                <Message className='icon'/>
                <Likes className='icon'/>
                <Send className='icon'/>
            </div>
            <BookMark className='icon'/>
        </div>
    )
}

export default CardMenu
