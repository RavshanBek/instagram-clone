import React from 'react';
import '../styles/footer.scss';
function Footer() {
    return (
        <div className='footer'>
            <ul className='links'>
                <li>About</li>
                <li>Help</li>
                <li>Press</li>
                <li>Api</li>
                <li>Jobs</li>
                <li>Privacy</li>
                <li>Terms</li>
                <li>Locations</li>
                <li>Top Accounts</li>
                <li>Hashtags</li>
                <li>Languages</li>
                <div className="copyright">
                    @ 2020 INSTAGRAM FROM FACEBOOK
                </div>
            </ul>
        </div>
    )
}

export default Footer
