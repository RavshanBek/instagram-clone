import React from 'react'
import '../styles/menu.scss';
import {ReactComponent as Home} from "../images/home.svg";
import {ReactComponent as Message} from "../images/message.svg";
import {ReactComponent as Likes} from "../images/likes.svg";
import {ReactComponent as Expolore} from "../images/expolore.svg";
import ProfileIcon from "./ProfileIcon";
import Ravshan from '../images/ravshan.jpg'
function Menu() {
    return (
        <div className='menu'>
             <Home className='icon'/> 
             <Message className='icon'/>
             <Likes className='icon'/>
             <Expolore className='icon'/>
             <ProfileIcon iconSize='small' image={Ravshan} storyBorder={true}/>
        </div>
    )
}

export default Menu
