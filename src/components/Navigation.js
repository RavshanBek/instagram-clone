import React from 'react'
import '../styles/navigation.scss';
import Logo from "../images/logo.png";
import search from "../images/search.png";
import Menu from "./Menu.js";
function Navigation() {
    return (
        <div className='navigation'>
             <div className="container">
                 <img src={Logo} alt="intagram logo" className="logo"/>
                 <div className="search">
                     <img src={search} alt="search icon " className="searchIcon"/>
                     <span className="searchText">Search</span>
                 </div>
                 <Menu />
             </div>
        </div>
    )
}

export default Navigation
